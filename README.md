# portainer

### Como executar
Caso não existir ainda, deve ser criado a persistência de dados:
```
mkdir /srv/deploy/portainer
```

Caso não tiver sido criado, deve ser criado a rede:
```
docker network create --driver=overlay traefik-net
```
